#include <eosiolib/asset.hpp>
#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/crypto.h>
#include <eosio.token/eosio.token.hpp>
#include <string>

using namespace eosio;

class eosiocompose : public eosio::contract {
	public:
		using contract::contract;
		
		/* --------- global variables ------- */
		
		/// accounts : bank, owner, receiver
		account_name owner = _self;
		account_name bank = N(eosio);
		account_name receiver;
		
		/// price of contract
		uint64_t price;
		
		/// Hash contract
		checksum256 contractHash;
		std::string status = "pending";
		bool hashIsSet = false;
		
		/* ----------- end globals ----------- */
		
		/// function to fix pricing
		uint64_t fixPricing(uint64_t quantity) {
			uint64_t fix = 100;
			uint64_t rate0 = 5;
			
			return fix + (quantity * rate0 / 100);
		}

		/// execution of compose contract
		void execute(uint64_t quantity, account_name receiverName) {
			eosio_assert(_self != receiverName, "cannot transfer to self" );
			eosio_assert(quantity > 0, "must transfer positive quantity" );
		
			// set globals 
			price = fixPricing(quantity);
			receiver = receiverName;
			setHash();
			
			setStatus("active");
			
			// call transfer actions
			transferAction(owner, bank, price*2*10000, "owner to bank");
			transferAction(bank, receiver, price*10000, "bank to receiver");
			transferAction(bank, owner, price*10000, "bank to owner");
			
			setStatus("accept");
			
			/* debug print */		
			print("Bank : ", name{bank}, "\n");
			print("Owner : ", name{owner}, "\n");
			print("Receiver : ", name{receiver}, "\n");
			print("Price : ", price, "\n");
			print("Status : ", status, "\n");
			print("Contract Hash : ");
			printhex(&contractHash, sizeof(contractHash));
		}
			
		/// transfer function : send tokens from account to another
		void transferAction(account_name from, account_name to, uint64_t amount, std::string memo) {
			require_auth(owner);
			
			action(
				permission_level {from, N(active)},
				N(eosio.token),N(transfer),
				std::make_tuple(from, to, asset(amount, symbol_type(S(4,SYS))), std::string(memo))
			).send();	
		}
			
		/*----------- getters and setters --------------*/
		
		/// set hash with sha256 method
		void setHash() {
			require_auth(owner);
			char buf[10];
			sha256(buf, 16, &contractHash);
			setHashIsSet(true);
		}
		
		/// get hash : return checksum256 hash
		checksum256 getHash() {
			return contractHash;
		}
		
		/// set value for hashIsSet : set true or false
		void setHashIsSet(bool value) {
			require_auth(owner);
			hashIsSet = value;
		}
		
		/// get value for hashIsSet : return true or false
		bool getHashIsSet() {
			return hashIsSet;
		}
		
		/// set value for status
		void setStatus(std::string value) {
			require_auth(owner);
			status = value;
		}
		
		/// get value for status
		std::string getStatus() {
			return status;
		}
		
		/// show balance of specific account
		void showBalance(account_name acc) {
			auto eos_token = eosio::token(N(eosio.token));
			auto balance = eos_token.get_balance(acc, symbol_type(S(4, SYS)).name());
			print("Balance: ", balance.amount, "\n");
		}
		
		/* ---------- end getters and setters ---------- */
};

EOSIO_ABI(eosiocompose, (execute))

