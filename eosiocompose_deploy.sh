#!/bin/bash

# Before execute read this:
ls
# Start your nodeos and keosd services
# Create eosiocompose folder into build/contracts/ and eosiocompose.cpp file into build/contracts/eosiocompose (and paste the source code)
# The arguments required must be like : http://ip:port
# The script must be executed in eos folder root
# Example execution in termial : ./eos_deploy.sh http://10.193.203.238:8888 http://10.193.203.238:8900
# Modify variables with your own informations if necessary

echo -e "#####################################################"
echo -e "###     Script for deploy eosiocompose contract   ###"
echo -e "#####################################################\n"

if [ $# -ne 2 ]; then
    echo -e "Usage: ./eos_deploy.sh {ip_nodeos} {ip_keosd}\n"
	exit 1
fi

cmd="cleos -u $1 --wallet-url $2"

# modify with your wallet info
walletName="network"
walletPass="PW5KUKTkLaokKP9EBQhK5Smeb5uR2pUVeVoWAEs8qdmvHot8JcK3s"

# modify with your main account or keep eosio
mainAccount="eosio"
keyAccount="EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV"

echo -e "Start execution...ok\n"

# unlock wallet
$cmd wallet unlock -n "$walletName" --password "$walletPass"

# create eosio.token account
$cmd create account "$mainAccount" eosio.token "$keyAccount" "$keyAccount"

# create eosio.msig account
#$cmd create account "$mainAccount" eosio.msig "$keyAccount" "$keyAccount"

# create eosio.system account
#$cmd create account "$mainAccount" eosio.system "$keyAccount" "$keyAccount"

# create eosio.bios account
#$cmd create account "$mainAccount" eosio.bios "$keyAccount" "$keyAccount"


# compile eosio.token in webAssembly 
#eosiocpp -o contracts/eosio.token/eosio.token.wast contracts/eosio.token/eosio.token.cpp

# generate eosio.token abi
#eosiocpp -g contracts/eosio.token/eosio.token.abi contracts/eosio.token/eosio.token.cpp

# set eosio.token contract in blockchain
$cmd set contract eosio.token build/contracts/eosio.token -p eosio.token

# set eosio.msig contract in blockchain
#$cmd set contract eosio.msig build/contracts/eosio.msig -p eosio.msig

# set eosio.system contract in blockchain
#$cmd set contract eosio.system build/contracts/eosio.system -p eosio.system

# set eosio.bios contract in blockchain
#$cmd set contract eosio.bios build/contracts/eosio.bios -p eosio.bios

# create token
$cmd push action eosio.token create '["eosio", "1000000000.0000 SYS", 0, 0, 0]' -p eosio.token -f

# send token to eosio account
$cmd push action eosio.token issue '["eosio", "100000.0000 SYS", "memo"]' -p eosio -f

# create user account
$cmd create account "$mainAccount" user "$keyAccount" "$keyAccount"

# send token to user account
$cmd push action eosio.token issue '["user", "100000.0000 SYS", "memo"]' -p eosio -f

# create eosiocompose account
$cmd create account "$mainAccount" eosiocompose "$keyAccount" "$keyAccount"

# send token to eosiocompose account
$cmd push action eosio.token issue '["eosiocompose", "100000.0000 SYS", "memo"]' -p eosio -f

#set permissions
$cmd set account permission eosio active '{"threshold":1,"keys":[{"key":"EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight":1}],"accounts":[{"permission":{"actor":"eosiocompose","permission":"eosio.code"},"weight":1}],"waits":[]}' owner -p eosio
$cmd set account permission eosiocompose active '{"threshold":1,"keys":[{"key":"EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","weight":1}],"accounts":[{"permission":{"actor":"eosiocompose","permission":"eosio.code"},"weight":1}],"waits":[]}' owner -p eosiocompose

# compile eosiocompose
eosiocpp -o build/contracts/eosiocompose/eosiocompose.wast build/contracts/eosiocompose/eosiocompose.cpp

# generate abi for eosiocompose
eosiocpp -g build/contracts/eosiocompose/eosiocompose.abi build/contracts/eosiocompose/eosiocompose.cpp

# deploy eosiocompose contract into blockchain
$cmd set contract eosiocompose build/contracts/eosiocompose

# test contract
$cmd push action eosiocompose execute '[3000, "user"]' -p eosiocompose@active

