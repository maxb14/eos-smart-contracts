#!/bin/bash

##############################################
#Script shell for deploy a node EOS on Ubuntu#
##############################################

echo -e "#########################################"
echo -e "Welcome to node EOS auto installation"
echo -e "#########################################\n"

#get ip current server
ip=$(hostname -I | awk '{print $1}')

#checkings
echo -e "Hello you're onnected on :" $ip
echo -e "#########################################"
echo -e "Your ip is good ? (yes or no)"
read yesno
if [ "$yesno" = "no" ]
then
	echo -e "What is your ip ?"
	read ip
fi

echo -e "#########################################"

echo -e "Hello what is your organization name ? (ex: cityscoot)"
read name
echo It\'s nice to meet you $name

echo -e "#########################################"

#wget eosio source for ubuntu-18
wget https://github.com/eosio/eos/releases/download/v1.5.0/eosio_1.5.0-1-ubuntu-18.04_amd64.deb

#install the deb package with sudo
sudo apt install ./eosio_1.5.0-1-ubuntu-18.04_amd64.deb

#create folder for installation
mkdir eosio

#change directory to check path installation
cd eosio

#copy path in variable
eospath=$(pwd)

#start keosd
keosd --http-server-address $ip:3000 \
--data-dir $eospath/wallet/config \
--config-dir $eospath/wallet/config >> $eospath/keosd.log 2>&1 &

#start nodeos
nodeos -e -p eosio \
--http-server-address $ip:8888 \
--plugin eosio::producer_plugin \
--plugin eosio::chain_api_plugin \
--plugin eosio::http_plugin \
--plugin eosio::history_plugin \
--plugin eosio::history_api_plugin \
--data-dir $eospath/node/data \
--config-dir $eospath/node/config \
--access-control-allow-origin='*' \
--contracts-console \
--http-validate-host=false \
--verbose-http-errors \
--filter-on='*' >> $eospath/nodeos.log 2>&1 &

#config client command
mycleos="cleos -u http://$ip:8888 --wallet-url http://$ip:3000"

#create wallet
$mycleos wallet create -n $name -f $eospath/wallet/walletpwd.txt
echo -e "Please save your wallet password and remove this walletpwd.txt"

echo -e "#########################################"

#open wallet
$mycleos wallet open -n $name

echo -e "#########################################"

passwallet=$(cat $eospath/wallet/walletpwd.txt)

#unlock wallet
$mycleos wallet unlock -n $name --password $passwallet

echo -e "#########################################"

#show wallet list
$mycleos wallet list

echo -e "#########################################"

$mycleos create key -f $eospath/key.txt

split -l 1 $eospath/key.txt

publickey=$(cut -d ' ' -f 3 $eospath/xab)
privatekey=$(cut -d ' ' -f 3 $eospath/xaa)

rm $eospath/xaa $eospath/xab

$mycleos wallet import -n $name --private-key $privatekey

$mycleos wallet import -n $name --private-key 5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3

sleep 1

echo -e "#########################################"

#create account
$mycleos create account eosio $name $publickey -p eosio@active

echo -e "#########################################"

#print
echo -e "NODEOS SERVICE : started on " $ip:8888
echo -e "KEOSD SERVICE : started on " $ip:3000
echo -e "For show details, please use : tail -f eosio/nodeos.log"

echo -e "#########################################"
